from pandas import read_csv
import numpy as np
from matplotlib import pyplot as plt


def load_data(filename):
    """
    Load data from a csv file
    :param filename: string corresponding to the filename to be loaded
    :return: X, the data matrix; y, the labels of each sample
    """
    data = read_csv(filename)
    z = np.array(data)
    y = z[:, 0]
    X = z[:, 1:]
    return X, y

filename = 'C:\Users\D.T\getdemo\Exe1\isde-1819-exe-1\data\mnist_data.csv'  # the location of the CSV file w.r.t. this script
X, y = load_data(filename)


def split_data(X, y, tr_fraction=0.5):
    """Split the data X,y into two random subsets."""
    tr_fraction = (0.5 * 2890)/100

    for i in xrange(tr_fraction):
        xtr = np.sum(X[i,:])
        ytr = y[i]
    for j in xrange(2890-tr_fraction):
        xts = np.sum(X[j,:])
        yts = sum(y[j])

    return xtr,ytr,xts,yts



def count_digits(y):
    """Count the number of elements in each class."""

    raise NotImplementedError


def fit(Xtr, ytr):
    """Compute the average centroid for each class."""
    raise NotImplementedError
